-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Apr 22, 2019 at 03:20 AM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.3.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `hitthebooks`
--

-- --------------------------------------------------------

--
-- Table structure for table `htb_fail`
--

CREATE TABLE `htb_fail` (
  `idFail` int(11) NOT NULL,
  `urlFail` varchar(255) NOT NULL,
  `tajukFail` varchar(255) NOT NULL,
  `tarikhCiptaFail` datetime NOT NULL,
  `uploader` varchar(255) NOT NULL,
  `idPengajar` int(11) DEFAULT NULL,
  `idPelajar` int(11) DEFAULT NULL,
  `idKemasukan` int(11) NOT NULL,
  `idSubjek` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `htb_fail`
--

INSERT INTO `htb_fail` (`idFail`, `urlFail`, `tajukFail`, `tarikhCiptaFail`, `uploader`, `idPengajar`, `idPelajar`, `idKemasukan`, `idSubjek`) VALUES
(6, '../uploads/Report E-Usahawanan.odt', 'Report Iqbal M1', '2019-04-17 10:03:18', 'SYED MOHAMMAD IQBAL BIN SYED AHMAD KAMAL', NULL, 22218087, 1, 1),
(7, '../uploads/distros-1024x768.jpeg', 'hshshs', '2019-04-17 12:57:54', 'SYED MOHAMMAD IQBAL BIN SYED AHMAD KAMAL', NULL, 22218087, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `htb_kemasukan`
--

CREATE TABLE `htb_kemasukan` (
  `idKemasukan` int(11) NOT NULL,
  `intake` varchar(7) NOT NULL,
  `kursus` varchar(255) NOT NULL,
  `tarikh_masuk` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `htb_kemasukan`
--

INSERT INTO `htb_kemasukan` (`idKemasukan`, `intake`, `kursus`, `tarikh_masuk`) VALUES
(1, '02/2018', 'Perisian', '2018-06-25'),
(2, '02/2019', 'Perisian', '2019-04-01');

-- --------------------------------------------------------

--
-- Table structure for table `htb_pelajar`
--

CREATE TABLE `htb_pelajar` (
  `idPelajar` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `ndp` varchar(8) NOT NULL,
  `intake` varchar(7) NOT NULL,
  `kursus` varchar(255) NOT NULL,
  `telefon` varchar(15) NOT NULL,
  `email` varchar(255) NOT NULL,
  `alamat` varchar(255) NOT NULL,
  `ic` varchar(14) NOT NULL,
  `idKemasukkan` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `htb_pelajar`
--

INSERT INTO `htb_pelajar` (`idPelajar`, `nama`, `ndp`, `intake`, `kursus`, `telefon`, `email`, `alamat`, `ic`, `idKemasukkan`) VALUES
(22218064, 'zaki', '22218064', '02/2019', 'Perisian', '012456639', 'zaki@zaki.comn', 'Klang', '909999-10-9999', 2),
(22218087, 'SYED MOHAMMAD IQBAL BIN SYED AHMAD KAMAL', '22218087', '02/018', 'Perisian', '01131116181', 'rpgiqbal@gmail.com', '1, JALAN TUN ABDUL RAZAK, TAMAN SRI ANDALAS, 41200 KLANG', '950429-10-6377', 1);

-- --------------------------------------------------------

--
-- Table structure for table `htb_pengajar`
--

CREATE TABLE `htb_pengajar` (
  `idPengajar` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `telefon` varchar(15) NOT NULL,
  `idStaff` varchar(10) NOT NULL,
  `department` varchar(255) NOT NULL,
  `ic` varchar(14) NOT NULL,
  `alamat` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `htb_pengajar`
--

INSERT INTO `htb_pengajar` (`idPengajar`, `nama`, `email`, `telefon`, `idStaff`, `department`, `ic`, `alamat`) VALUES
(81015, 'Hazlina', 'hazlinas@gmail.com', '0123456789', '', 'PERISIAN', '785463-10-8462', 'Bilik Pengajar Perisian'),
(11100001, 'M. Helmi Bin M. Aris', 'helmi@iljtm.com.my', '0123456789', '', 'PERISIAN', '970125-14-5594', 'Bilik Pengajar Rangkaian'),
(11100007, '', '', '', '', 'PERISIAN', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `htb_subjek`
--

CREATE TABLE `htb_subjek` (
  `idSubjek` int(11) NOT NULL,
  `idPengajar` int(11) NOT NULL,
  `namaSubjek` varchar(50) NOT NULL,
  `kodSubjek` varchar(10) NOT NULL,
  `idKemasukan` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `htb_subjek`
--

INSERT INTO `htb_subjek` (`idSubjek`, `idPengajar`, `namaSubjek`, `kodSubjek`, `idKemasukan`) VALUES
(1, 11100001, 'Visual Basic 1', 'F04-01-01', 1),
(2, 11100001, 'HTML', '12345', 1),
(3, 81015, 'FYP', '12345', 2);

-- --------------------------------------------------------

--
-- Table structure for table `htb_user`
--

CREATE TABLE `htb_user` (
  `idUser` int(11) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(255) NOT NULL,
  `role` int(11) DEFAULT NULL,
  `idPelajar` int(11) DEFAULT NULL,
  `idPengajar` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `htb_user`
--

INSERT INTO `htb_user` (`idUser`, `username`, `password`, `role`, `idPelajar`, `idPengajar`) VALUES
(1, 'admin', '21232f297a57a5a743894a0e4a801fc3', NULL, NULL, NULL),
(2, 'helmi', '21232f297a57a5a743894a0e4a801fc3', 1, NULL, 11100001),
(4, 'iqbal', 'eedae20fc3c7a6e9c5b1102098771c70', 2, 22218087, NULL),
(5, 'hazlina', '59bc1e17b87915727e2a2ff522348ca4', 1, NULL, 81015),
(6, 'zaki', '9784ea3da268563469df99b2e6593564', 2, 22218064, NULL),
(7, 'naoto', '827ccb0eea8a706c4c34a16891f84e7b', 1, NULL, 11100007);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `htb_fail`
--
ALTER TABLE `htb_fail`
  ADD PRIMARY KEY (`idFail`),
  ADD KEY `idSubjek` (`idSubjek`),
  ADD KEY `idKemasukan` (`idKemasukan`),
  ADD KEY `idPengajar` (`idPengajar`,`idKemasukan`,`idSubjek`) USING BTREE,
  ADD KEY `htb_fail_ibfk_4` (`idPelajar`);

--
-- Indexes for table `htb_kemasukan`
--
ALTER TABLE `htb_kemasukan`
  ADD PRIMARY KEY (`idKemasukan`);

--
-- Indexes for table `htb_pelajar`
--
ALTER TABLE `htb_pelajar`
  ADD PRIMARY KEY (`idPelajar`),
  ADD KEY `idKemasukkan` (`idKemasukkan`) USING BTREE;

--
-- Indexes for table `htb_pengajar`
--
ALTER TABLE `htb_pengajar`
  ADD PRIMARY KEY (`idPengajar`);

--
-- Indexes for table `htb_subjek`
--
ALTER TABLE `htb_subjek`
  ADD PRIMARY KEY (`idSubjek`),
  ADD KEY `idPengajar` (`idPengajar`) USING BTREE,
  ADD KEY `idKemasukan` (`idKemasukan`) USING BTREE;

--
-- Indexes for table `htb_user`
--
ALTER TABLE `htb_user`
  ADD PRIMARY KEY (`idUser`),
  ADD UNIQUE KEY `idPelajar` (`idPelajar`,`idPengajar`),
  ADD KEY `idPengajar` (`idPengajar`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `htb_fail`
--
ALTER TABLE `htb_fail`
  MODIFY `idFail` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `htb_kemasukan`
--
ALTER TABLE `htb_kemasukan`
  MODIFY `idKemasukan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `htb_pelajar`
--
ALTER TABLE `htb_pelajar`
  MODIFY `idPelajar` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22218088;

--
-- AUTO_INCREMENT for table `htb_pengajar`
--
ALTER TABLE `htb_pengajar`
  MODIFY `idPengajar` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11100008;

--
-- AUTO_INCREMENT for table `htb_subjek`
--
ALTER TABLE `htb_subjek`
  MODIFY `idSubjek` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `htb_user`
--
ALTER TABLE `htb_user`
  MODIFY `idUser` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `htb_fail`
--
ALTER TABLE `htb_fail`
  ADD CONSTRAINT `htb_fail_ibfk_1` FOREIGN KEY (`idPengajar`) REFERENCES `htb_pengajar` (`idPengajar`),
  ADD CONSTRAINT `htb_fail_ibfk_2` FOREIGN KEY (`idSubjek`) REFERENCES `htb_subjek` (`idSubjek`),
  ADD CONSTRAINT `htb_fail_ibfk_3` FOREIGN KEY (`idKemasukan`) REFERENCES `htb_kemasukan` (`idKemasukan`),
  ADD CONSTRAINT `htb_fail_ibfk_4` FOREIGN KEY (`idPelajar`) REFERENCES `htb_pelajar` (`idPelajar`);

--
-- Constraints for table `htb_pelajar`
--
ALTER TABLE `htb_pelajar`
  ADD CONSTRAINT `htb_pelajar_ibfk_1` FOREIGN KEY (`idKemasukkan`) REFERENCES `htb_kemasukan` (`idKemasukan`) ON DELETE CASCADE;

--
-- Constraints for table `htb_subjek`
--
ALTER TABLE `htb_subjek`
  ADD CONSTRAINT `htb_subjek_ibfk_1` FOREIGN KEY (`idKemasukan`) REFERENCES `htb_kemasukan` (`idKemasukan`) ON DELETE CASCADE,
  ADD CONSTRAINT `htb_subjek_ibfk_2` FOREIGN KEY (`idPengajar`) REFERENCES `htb_pengajar` (`idPengajar`) ON DELETE CASCADE;

--
-- Constraints for table `htb_user`
--
ALTER TABLE `htb_user`
  ADD CONSTRAINT `htb_user_ibfk_1` FOREIGN KEY (`idPelajar`) REFERENCES `htb_pelajar` (`idPelajar`) ON DELETE CASCADE,
  ADD CONSTRAINT `htb_user_ibfk_2` FOREIGN KEY (`idPengajar`) REFERENCES `htb_pengajar` (`idPengajar`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
