<?php

	include '../../php/connection.php';
	session_start();

	$idSubjek = $_GET['idSubjek'];
	$sqlKemasukan = "SELECT * FROM htb_subjek;";
	$resultKemasukan = mysqli_query($conn, $sqlKemasukan);
	$rowKemasukan = mysqli_fetch_assoc($resultKemasukan);
	$idKemasukan = $rowKemasukan['idKemasukan'];

	//fetching user's ID
	$rawUser = $_SESSION['username'];
	$sqlFetchIdPengajar = "SELECT * FROM htb_user INNER JOIN htb_pengajar ON htb_user.idPengajar = htb_pengajar.idPengajar WHERE username = '$rawUser';";

	$resultFetch = mysqli_query($conn, $sqlFetchIdPengajar);

	$resP = mysqli_fetch_assoc($resultFetch);

	$idPengajar = $resP['idPengajar'];
	$namaUploader = $resP['nama'];

	//datetime function
	date_default_timezone_set('Asia/Kuala_Lumpur');
	$mysqltime = date ("Y-m-d H:i:s");

	//at the end so that $idPengajar could be borrowed by procAddFail.php
	include '../../php/procAddFail.php';

?>
<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Hit The Books | Pengajar</title>

  <!-- Custom fonts for this template-->
  <link href="/projects/hitthebooksfilesharing/css/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="/projects/hitthebooksfilesharing/css/sb-admin-2.min.css" rel="stylesheet">

</head>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

    <!-- Sidebar -->
    <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

      <!-- Sidebar - Brand -->
      <a class="sidebar-brand d-flex align-items-center justify-content-center" href="/projects/hitthebooksfilesharing/core/admin/ad-main.php">
        <div class="sidebar-brand-icon">
          <!--<i class="fas fa-laugh-wink"></i>-->
		<img src="/projects/hitthebooksfilesharing/img/JTM-2014.png" alt="Logo ILJTM" width="50">
        </div>
	   <br>
        <div class="sidebar-brand-text mx-3">Pengajar</div>
      </a>

      <!-- Divider -->
      <hr class="sidebar-divider my-0">

      <!-- Nav Item - Dashboard -->
      <li class="nav-item">
        <a class="nav-link" href="/projects/hitthebooksfilesharing/core/pengajar/pgjr-main.php">
          <i class="fas fa-fw fa-tachometer-alt"></i>
          <span>Dashboard</span></a>
      </li>

      <!-- Nav Item - Pengajar Collapse Menu -->
     <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
          <i class="fas fa-fw fa-folder"></i>
          <span>Subjek</span>
        </a>
        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">Bahagian Subjek:</h6>
            <a class="collapse-item" href="pgjr-addsubjek.php">Tambah Subjek</a>
          </div>
        </div>
  </li>


      <!-- Sidebar Toggler (Sidebar) -->
      <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
      </div>

    </ul>
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <!-- Topbar -->
        <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

          <!-- Sidebar Toggle (Topbar) -->
          <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
            <i class="fa fa-bars"></i>
          </button>


          <!-- Topbar Navbar -->
          <ul class="navbar-nav ml-auto">

            <div class="topbar-divider d-none d-sm-block"></div>

            <!-- Dropdown - User Information -->
            <li class="nav-item dropdown no-arrow">
             <a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal">
			<i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
			Logout
		   </a>
            </li>

          </ul>

        </nav>
        <!-- End of Topbar -->

        <!-- Begin Page Content -->
        <div class="container-fluid">
            <!-- CARD TABLE -->
            <div class="card shadow mb-4">
                <div class="card-header">
                   <h3>
				    <span>
   				   <a href="/projects/hitthebooksfilesharing/core/pengajar/pgjr-lihatsubjek.php?idSubjek=<?php echo $idSubjek ?>" class="btn btn-success btn-circle btn-sm">
   					   <i class="fa fa-arrow-left"></i>
   				   </a>
   			   </span>
				    Tambah Fail
                    </h3>
                </div>
                <div class="card-body">
                        <form method="post" enctype="multipart/form-data">
					  <div class="form-group">
						  <input type="file" name="failUpload" required>
						  <br><br>
						  <label>Nama</label>
						  <input type="text" name="tajukFail" placeholder="Nama Fail" class="form-control" required>
					  </div>
					  <div class="card-footer">
						  <button type="submit" class="btn btn-primary">Tambah</button>
						  <a href="pgjr-lihatsubjek.php?idSubjek=<?php echo $idSubjek; ?>" class="btn btn-secondary">Cancel</a>
					  </div>
                        </form>
                </div>
            </div>
            <!-- / CARD TABLE -->


        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

      <!-- Footer -->
      <footer class="sticky-footer bg-white">
        <div class="container my-auto">
          <div class="copyright text-center my-auto">
            <span>Copyright &copy; Your Website 2019</span>
          </div>
        </div>
      </footer>
      <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <!-- Logout Modal-->
  <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
          <a class="btn btn-primary" href="/projects/hitthebooksfilesharing/php/logout.php">Logout</a>
        </div>
      </div>
    </div>
  </div>

<!-- THIS IS FOR FOOTER
  <!-- Bootstrap core JavaScript-->
  <script src="/projects/hitthebooksfilesharing/js/jquery.min.js"></script>
  <script src="/projects/hitthebooksfilesharing/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="/projects/hitthebooksfilesharing/js/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="/projects/hitthebooksfilesharing/js/sb-admin-2.min.js"></script>

</body>

</html>
-->
