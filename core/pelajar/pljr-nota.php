<?php

	include '../../php/connection.php';
	session_start();

	$idSubjek = $_GET['idSubjek'];
	$username = $_SESSION['username'];

	$sqlChangeToID = "SELECT * FROM htb_user WHERE username = '$username';";
	$resultChangeToID = mysqli_query($conn, $sqlChangeToID);
	$resCTID = mysqli_fetch_assoc($resultChangeToID);
	$idUser = $resCTID['idPelajar'];

?>


<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Hit The Books | Pelajar</title>

  <!-- Custom fonts for this template-->
  <link href="/projects/hitthebooksfilesharing/css/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="/projects/hitthebooksfilesharing/css/sb-admin-2.min.css" rel="stylesheet">

</head>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

    <!-- Sidebar -->
    <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

      <!-- Sidebar - Brand -->
      <a class="sidebar-brand d-flex align-items-center justify-content-center" href="pljr-main.php">
        <div class="sidebar-brand-icon">
          <!--<i class="fas fa-laugh-wink"></i>-->
		<img src="/projects/hitthebooksfilesharing/img/JTM-2014.png" alt="Logo ILJTM" width="50">
        </div>
        <div class="sidebar-brand-text mx-3">Pelajar</div>
      </a>

      <!-- Divider -->
      <hr class="sidebar-divider my-0">

      <!-- Nav Item - Dashboard -->
      <li class="nav-item">
        <a class="nav-link" href="pljr-main.php">
          <i class="fas fa-fw fa-tachometer-alt"></i>
          <span>Dashboard</span></a>
      </li>

      <!-- Nav Item - Pengajar Collapse Menu -->
      <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
          <i class="fas fa-fw fa-folder"></i>
          <span>Pelajar Corner</span>
        </a>
        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">Bahagian Pelajar:</h6>
            <a class="collapse-item" href="pljr-hasilkerja.php">E-nota</a>
            <a class="collapse-item" href="pljr-nota.php">Hasil Kerja</a>
          </div>
        </div>
    </li>


      <!-- Sidebar Toggler (Sidebar) -->
      <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
      </div>

    </ul>
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <!-- Topbar -->
        <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

          <!-- Sidebar Toggle (Topbar) -->
          <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
            <i class="fa fa-bars"></i>
          </button>


          <!-- Topbar Navbar -->
          <ul class="navbar-nav ml-auto">

            <!-- Nav Item - Search Dropdown (Visible Only XS) -->
            <li class="nav-item dropdown no-arrow d-sm-none">
              <a class="nav-link dropdown-toggle" href="#" id="searchDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-search fa-fw"></i>
              </a>
              <!-- Dropdown - Messages -->
              <div class="dropdown-menu dropdown-menu-right p-3 shadow animated--grow-in" aria-labelledby="searchDropdown">
                <form class="form-inline mr-auto w-100 navbar-search">
                  <div class="input-group">
                    <input type="text" class="form-control bg-light border-0 small" placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2">
                    <div class="input-group-append">
                      <button class="btn btn-primary" type="button">
                        <i class="fas fa-search fa-sm"></i>
                      </button>
                    </div>
                  </div>
                </form>
              </div>
            </li>

            <div class="topbar-divider d-none d-sm-block"></div>

            <!-- Nav Item - User Information -->
            <li class="nav-item dropdown no-arrow">
              <a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal">
			 <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
			 Logout
		    </a>
            </li>

          </ul>

        </nav>
        <!-- End of Topbar -->

         <!-- Begin Page Content -->
        <div class="container-fluid">
                <!-- CARD TABLE -->
                <div class="card shadow mb-4">
                    <div class="card-header">
                       <h3>Upload Hasil Kerja
                       </h3>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md">
                                <div class="container-fluid">
                                    <h3>Upload
    						<span>
    						  <a href="tambahFailPelajar.php?idSubjek=<?php echo $idSubjek; ?>" class="btn btn-success btn-circle btn-sm"><i class="fas fa-plus"></i></a>
    						</span></h3>
                                    <hr>
                                    <div class="table-responsive">
    							  <?php

    							  	$sqlViewFile = "SELECT * FROM htb_fail WHERE idSubjek = '$idSubjek' ORDER BY tarikhCiptaFail ASC;";
    								$resViewFile = mysqli_query($conn, $sqlViewFile);



    							  ?>
    								<table class="table table-bordered" width="100%" cellspacing="0" id="DataTable">
    								    <thead>
    									   <tr>
    										  <th>Tajuk Subjek</th>
    										  <th>Nama Pengupload</th>
    										  <th>Tarikh Upload</th>
    										  <th>Action</th>
    									   </tr>
    								    </thead>
    								    <tfoot>
    									   <tr>
    										  <th>Tajuk Subjek</th>
    										  <th>Nama Pengupload</th>
    										  <th>Tarikh Upload</th>
    										  <th>Action</th>
    									   </tr>
    								    </tfoot>
    								    <tbody>
    									    <?php

    									    		while ($resVF = mysqli_fetch_assoc($resViewFile)) {

    												$idFail = $resVF['idFail'];
    												$urlFail = $resVF['urlFail'];
												$idPelajar = $resVF['idPelajar'];

    												$tarikhCiptaFail = $resVF['tarikhCiptaFail'];
    												$tajukFail = $resVF['tajukFail'];
    												$uploader = $resVF['uploader'];


    									    ?>
    									    <tr>
    										  <td><?php echo $tajukFail; ?></td>
    										  <td><?php echo $uploader; ?></td>
    										  <td><?php echo $tarikhCiptaFail; ?></td>
    										  <td>
    											 <a href="<?php echo $urlFail; ?>" class="btn btn-success btn-circle btn-sm"><i class="fa fa-download" aria-hidden="true"></i></a href="">
												 <?php

												 	if ($idPelajar == $idUser) {
												 		?>
														<a href="deleteFilePelajar.php?idFail=<?php echo $idFail; ?>" class="btn btn-danger btn-circle btn-sm" style="color:white;"><i class="fas fa-trash"></i></a>
														<?php
												 	}

												 ?>

    										  </td>
    									   </tr>
    									   <?php } ?>

    								    </tbody>
    								</table>
    							 </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- / CARD TABLE -->


            </div>
            <!-- /.container-fluid -->

          </div>
          <!-- End of Main Content -->

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <!-- Logout Modal-->
  <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
          <a class="btn btn-primary" href="/projects/hitthebooksfilesharing/php/logout.php">Logout</a>
        </div>
      </div>
    </div>
  </div>

  <!-- Bootstrap core JavaScript-->
  <script src="/projects/hitthebooksfilesharing/js/jquery.min.js"></script>
  <script src="/projects/hitthebooksfilesharing/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="/projects/hitthebooksfilesharing/js/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="/projects/hitthebooksfilesharing/js/sb-admin-2.min.js"></script>

</body>

</html>
