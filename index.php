<!DOCTYPE html>
<html lang="en">
  <head> 
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Hit The Book- Main Page</title>

        <!-- Custom fonts for this template-->
        <link href="/projects/hitthebooksfilesharing/css/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

        <!-- Custom styles for this template-->
        <link href="/projects/hitthebooksfilesharing/css/sb-admin-2.min.css" rel="stylesheet">
        <link href="/projects/hitthebooksfilesharing/css/custom.css" rel="stylesheet">
        <link href="/projects/hitthebooksfilesharing/css/bootstrap4-harbor.min.css" rel="stylesheet">

  </head>
  <body>

  <div class="bg-info navbar-dark text-white">
    <div class="container">
      <nav class="navbar px-0 navbar-expand-lg navbar-dark">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
          <div class="navbar-nav">
            <a href="/projects/hitthebooksfilesharing/mainindex.html" class="pl-md-0 p-3 text-light">Home</a>
            <a href="core/login.php" class="p-3 text-decoration-none text-light">Login</a>
          </div>
        </div>
      </nav>

    </div>
  </div>




<div class="jumbotron jumbotron-harbor text-white">
  <div class="container">

      <h1 class="display-1 text-light">Hit-The-Books</h1>
      <span class="lead">Log Masuk bagi Warga Perisian ILP Selandar</span>

      <div class="mt-4">
        <a href="core/login.php"
           class="btn btn-primary btn-pill btn-wide btn-lg my-2">
          <span>Log Masuk</span>
        </a>
      </div>

  </div>
</div>

<div class="container py-5">
  <h1>Bantuan?</h1>

  <p>Untuk bantuan, ralat atau penambahbaikkan kepada Sistem Hit-The-Books sila hubungi<br>
    <a href="https://gitlab.com/pykekaisora/hitthebooksfilesharing">https://gitlab.com/pykekaisora/hitthebooksfilesharing</a><br>
  <p style="text-align:middle;">Made by Pyke Kaisora & Karlos D Cracker</p>
  </p>


</div>
    <!-- Bootstrap core JavaScript-->
  <script src="/projects/hitthebooksfilesharing/jquery/jquery.min.js"></script>
  <script src="/projects/hitthebooksfilesharing/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="/projects/hitthebooksfilesharing/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="js/sb-admin-2.min.js"></script>

  </body>
</html>
