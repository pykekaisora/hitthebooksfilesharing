<?php

	session_start();
	//to connect to database
	include ('connection.php');

	//to define index without isset and !empty, there will be error if there's
	//username nor password
	if (isset($_POST) && !empty($_POST)) {

		#$_POST is fetch from FORM method post so it won't show in the url.
		$username = $_POST['username'];
		$password = md5($_POST['password']);

		$sqlLog = "SELECT * FROM `htb_user` WHERE username='$username' AND password='$password'";

		#To submit SQL Query into database to show only the queries inside
		#$sqlLog.
		$result = mysqli_query($conn, $sqlLog);

		#to make sure that the person is indeed the user.
		#Because using the AND operand in sQL Query, there should only be one
		#person that has the username.
		$count = mysqli_num_rows($result);

		#$role is to fetch columns from database
		$role = mysqli_fetch_assoc($result);

		if ($count == 1) {
			$_SESSION['username'] = $username;

			if ($role['role'] == null ) {
				header('location: admin/ad-main.php');
			}

			elseif ($role['role'] == '2' ) {
				header('location: pelajar/pljr-main.php');
			}

			elseif ($role['role'] == '1' ) {
				header('location: pengajar/pgjr-main.php');
			}

			else {
				echo "You are not recognized by our system, kindly contact tech support for assistance";
			}
		}

		else {
			echo "Invalid Username/Password";
		}

	}

	#if (isset($_SESSION['username'])) {
	#	echo "User already logged in";
	#}

?>
